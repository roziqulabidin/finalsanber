<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCRUD;
use App\CategoryCRUD;

class ProductController extends Controller
{
    public function ProductShow()
    {
        $products['products'] = ProductCRUD::all();
        return view('HalamanAdmin.pages.product', ['products' => $products]);
    }

    public function ProductEdit($id_prd)
    {
        $products = ProductCRUD::where('id_prd', $id_prd)->get();
        // dd($products);
        // $category = CategoryCRUD::select()->where('ctg_id', $products->ctg_id)->get();
        $categories = CategoryCRUD::all();
        return view('HalamanAdmin.pages.EditData.edit-product', compact('products', 'categories'));
    }

    public function ProductUpdate(Request $request)
    {
        $products = ProductCRUD::where('id_prd', $request->id_prd)->update([
            'ctg_id' => $request->ctg_id,
            'img_prd' => $request->img_prd,
            'nama_prd' => $request->nama_prd,
            'hrg_prd' => $request->hrg_prd,
            'garansi_prd' => $request->garansi_prd,
            'processor_prd' => $request->processor_prd,
            'memory_prd' => $request->memory_prd,
            'storage_prd' => $request->storage_prd,
            'graphic_prd' => $request->graphic_prd,
            'display_prd' => $request->display_prd,
            'os_prd' => $request->os_prd,
        ]);
        return redirect('/Admin/Product/Show')->with('success', 'Data berhasil diupdate ! !');
    }

    public function ProductAdd()
    {
        $categories['categories'] = CategoryCRUD::all();
        return view('HalamanAdmin.pages.CreateData.create-product', ['categories'=>$categories]);
    }

    public function ProductSave(Request $request)
    {
        $products = ProductCRUD::create([
            'ctg_id' => $request->ctg_id,
            'img_prd' => $request->img_prd,
            'nama_prd' => $request->nama_prd,
            'hrg_prd' => $request->hrg_prd,
            'garansi_prd' => $request->garansi_prd,
            'processor_prd' => $request->processor_prd,
            'memory_prd' => $request->memory_prd,
            'storage_prd' => $request->storage_prd,
            'graphic_prd' => $request->graphic_prd,
            'display_prd' => $request->display_prd,
            'os_prd' => $request->os_prd,
        ]);

        return redirect('Admin/Product/Show')->with('success', 'Data berhasil ditambahkan ! !');
    }
}
